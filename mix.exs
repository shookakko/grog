defmodule Grog.MixProject do
  use Mix.Project

  def project do
    [
      app: :grog,
      version: "0.1.0",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Grog.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.8"},
      {:floki, "~> 0.33.1"},
      {:jason, "~> 1.3"},
      {:sizeable, "~> 1.0"},
      {:plug_cowboy, "~> 2.5"}
    ]
  end
end
