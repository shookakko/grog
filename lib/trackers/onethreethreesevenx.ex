defmodule Onethreethreesevenx do


  def generate_search_link(query) do
    url = Grog.get_tracker(:"1337x")[:url]
    encode_query = URI.encode(query)
    "#{url}search/#{encode_query}/"
  end

  def get_page(query,number)do
    "#{generate_search_link(query)}#{number}/"
    |> Grog.parse_html
  end

  def total_pages(page) do
    page = Floki.attribute(page, "li.last a", "href")
    |> List.first

    if page do
      page
      |> String.split("/")
      |> Enum.take(-2)
      |> List.first
      |> String.to_integer
    else 1
    end
  end

  def get_page_links(page) do
    links = Floki.find(page,"td.name > a")

    for link <- links do
      torrent = link
      |> Floki.attribute("href")

      if String.contains?(List.first(torrent),"torrent") do
	name = link
	|> Floki.text

	%{:href => torrent |> List.first , :name => name}
	
      end
    end
    |> Enum.filter(& !is_nil(&1))
    
  end

  def search(query) do
    first_page = get_page(query,1)
    total_pages = total_pages(first_page)

    cond do
      total_pages == 1 ->
	[get_page_links(first_page)]
	
	true ->
	for n <- 1..total_pages do
	    Task.async(fn -> get_page(query,n) |> get_page_links end)
	end
	|> Enum.map(
          &try do
            Task.await(&1, :infinity)
          catch
            _value -> %{"com" => ""}
          end)
    end
  end


  def get_torrent_info(href) do
    url = Grog.get_tracker(:"1337x")[:url]
    link = "#{url}#{String.slice(href,1..-1//1)}"

    torrent_page = Grog.parse_html(link)

    %{:magnet => torrent_page
      |> Floki.find("a[onclick]")
      |> List.first
      |> Floki.attribute("href")
      |> List.first,

      :size => torrent_page
      |> Floki.find("ul.list")
      |> Enum.take(-2)
      |> Floki.find("li")
      |> Enum.take(4)
      |> List.last
      |> Floki.find("span")
      |> Floki.text ,

      :files => torrent_page
      |> Floki.find("#files li")
      |> Enum.count ,

      :seeds => torrent_page
      |> Floki.find(".seeds")
      |> Floki.text
      |> String.to_integer,

      :leeches => torrent_page
      |> Floki.find(".leeches")
      |> Floki.text
      |> String.to_integer}


  end
  




end
