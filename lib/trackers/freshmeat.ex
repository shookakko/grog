defmodule Freshmeat do

  defp generate_search_link(query) do
    url = Grog.get_tracker(:freshmeat)[:url]
    encoded_query = URI.encode(query)
    "#{url}s?q=#{encoded_query}"
  end

  defp get_page(query, number) do
    "#{generate_search_link(query)}&skip=#{number*100}"
    |> Grog.parse_html
  end

  defp total_pages(first_page) do
    {results, _} = first_page
    |> Floki.find("div.mt-2")
    |> Floki.text
    |> String.split(["(", ")", " "], trim: true)
    |> List.last
    |> Integer.parse
    div(results, 100)
  end

  defp parse_html_result(html) do
    for res <- Floki.find(html, "tr.link > td > a") do
      href = Floki.attribute(res, "href")
      |> List.first
      |> String.slice(1..-1//1)
      name = Floki.text(res) |> String.trim
      %{href: href,
	name: name}
    end
  end

  def search(query) do
    first_page = get_page(query, 0)
    total_pages = total_pages(first_page)

    if total_pages > 0 do
      for page <- 0..total_pages do
	Task.async(fn -> get_page(query, page) |> parse_html_result end)
      end
      |> Enum.map(
      fn task ->
	try do
	  Task.await(task, :infinity)
	catch
	  _value -> nil
	end
      end)
    else
      [first_page
      |> parse_html_result]
    end
  end

  def get_torrent_info(href) do
    torrent_link = Grog.get_tracker(:freshmeat)[:url] <> href
    torrent_page = Grog.parse_html(torrent_link)
    if torrent_page do
      magnet = torrent_page
      |> Floki.find("div.col-12 > a.btn")
      |> List.first
      |> Floki.attribute("href")
      |> List.first
      |> URI.decode

      size = torrent_page
      |> Floki.find("div.col-12 p.badge")
      |> List.first
      |> Floki.text
      |> String.slice(6..-1//1)

      {files, _} = torrent_page
      |> Floki.find("div.col-12 > ul > li > a > small")
      |> List.first
      |> Floki.text
      |> Integer.parse

      %{magnet: magnet,
	size: size,
	files: files,
	seeds: nil,
	leechers: nil}
    end
  end

end
