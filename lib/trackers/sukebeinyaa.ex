defmodule SukebeiNyaa do

  defp generate_search_link(query) do
    url = Grog.get_tracker(:sukebeinyaa)[:url]
    encoded_query = URI.encode(query)
    "#{url}?f=0&c=0_0&q=#{encoded_query}"
  end

  defp get_page(query, number) do
    "#{generate_search_link(query)}&p=#{number}"
    |> Grog.parse_html
  end

  defp total_pages(first_page) do
    last_page = first_page
    |> Floki.find("ul.pagination > li > a")
    |> Enum.take(-2)
    |> List.first

    if last_page do
      {num, _} = last_page
      |> Floki.text
      |> Integer.parse
      num
    else
      1
    end
  end

  def parse_html_result(html) do
    for res <- Floki.find(html, "tbody > tr > td[colspan=\"2\"] > a:last-child") do
      href = Floki.attribute(res, "href")
      |> List.first
      |> String.slice(1..-1//1)
      name = Floki.attribute(res, "title")
      |> List.first
      %{href: href,
	name: name}
    end 
  end

  def search(query) do
    first_page = get_page(query, 1)
    total_pages = total_pages(first_page)

    if total_pages > 1 do
      for page <- 1..total_pages do
	Task.async(fn -> get_page(query, page) |> parse_html_result end)
      end
      |> Enum.map(
      fn task ->
	try do
	  Task.await(task, :infinity)
	catch
	  _value -> nil
	end
      end)
    else
      [first_page
      |> parse_html_result]
    end
  end

  def get_torrent_info(href) do
    torrent_link = Grog.get_tracker(:sukebeinyaa)[:url] <> href
    torrent_page = Grog.parse_html(torrent_link)
    if torrent_page do
      magnet = torrent_page
      |> Floki.find("div.panel-footer > a.card-footer-item")
      |> Floki.attribute("href")
      |> List.first
      |> URI.decode

      size = torrent_page
      |> Floki.find("div.panel-body > div.row:nth-of-type(4) > div.col-md-5:nth-of-type(2)")
      |> Floki.text

      files = torrent_page
      |> Floki.find("i.fa-file")
      |> Enum.count

      {seeds, _} = torrent_page
      |> Floki.find("div.panel-body > div.row:nth-of-type(2) > div.col-md-5:nth-of-type(4)")
      |> Floki.text
      |> Integer.parse

      {leechers, _} = torrent_page
      |> Floki.find("div.panel-body > div.row:nth-of-type(3) > div.col-md-5:nth-of-type(4)")
      |> Floki.text
      |> Integer.parse

      %{magnet: magnet,
	size: size,
	files: files,
	seeds: seeds,
	leechers: leechers}
    end
  end
end
