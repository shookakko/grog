defmodule ThePirateBay do

  @tpb_trackers "&tr=udp://tracker.coppersurfer.tk:6969/announce&tr=udp://tracker.openbittorrent.com:6969/announce&tr=udp://9.rarbg.to:2710/announce&tr=udp://9.rarbg.me:2780/announce&tr=udp://9.rarbg.to:2730/announce&tr=udp://tracker.opentrackr.org:1337&tr=http://p4p.arenabg.com:1337/announce&tr=udp://tracker.torrent.eu.org:451/announce&tr=udp://tracker.tiny-vps.com:6969/announce&tr=udp://open.stealth.si:80/announce"

  defp generate_search_link(query) do
    url = Grog.get_tracker(:thepiratebay)[:url]
    encoded_query = URI.encode(query)
    "#{url}q.php?q=#{encoded_query}"
  end

  defp parse_json_result(json) do
    for res <- json do
      href = "t.php?id=#{Map.get(res, "id")}"
      name = Map.get(res, "name")
      %{href: href,
	name: name}
    end
  end

  def search(query) do
    generate_search_link(query)
    |> Grog.parse_json
    |> parse_json_result
  end

  def get_torrent_info(href) do
    torrent_link = Grog.get_tracker(:thepiratebay)[:url] <> href
    torrent_page = Grog.parse_json(torrent_link)
    if Map.get(torrent_page, "id") != 0 do
      magnet = "magnet:?xt=urn:btih:" <>
	Map.get(torrent_page, "info_hash") <>
	"&dn=" <>
	Map.get(torrent_page, "name") <>
	@tpb_trackers

      size = torrent_page
      |> Map.get("size")
      |> Sizeable.filesize

      files = torrent_page
      |> Map.get("num_files")

      seeds = torrent_page
      |> Map.get("seeders")

      leechers = torrent_page
      |> Map.get("leechers")

      %{magnet: magnet,
	size: size,
	files: files,
	seeds: seeds,
	leechers: leechers}
    end
  end

end
