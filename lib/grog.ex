defmodule Grog do
  
  @trackers %{:"1337x"      => %{url: "https://1337x.to/",
				 description: "1337X is a Public torrent site that offers verified torrent downloads",
				 language: "en-US",
				 module: %{:search => &Onethreethreesevenx.search/1,
					   :torrent_info => &Onethreethreesevenx.get_torrent_info/1}},

	      :freshmeat    => %{url: "https://freshmeat.io/",
				 description:  "freshMeat is a Public torrent meta-search engine",
				 language: "en-US",
				 module: %{search: &Freshmeat.search/1,
					   torrent_info: &Freshmeat.get_torrent_info/1}},

	      :thepiratebay => %{url: "https://apibay.org/",
				 description: "The Pirate Bay (TPB) is the galaxy’s most resilient Public BitTorrent site",
				 language: "en-US",
				 module: %{search: &ThePirateBay.search/1,
					   torrent_info: &ThePirateBay.get_torrent_info/1}},

	      :nyaa         => %{url: "https://nyaa.si/",
				 description: "Nyaa is a Public torrent site focused on Eastern Asian media including anime, manga, literature and music",
				 language: "en-US",
				 module: %{search: &Nyaa.search/1,
					   torrent_info: &Nyaa.get_torrent_info/1}},

	      :sukebeinyaa  => %{url: "https://sukebei.nyaa.si/",
				 description: "sukebei.nyaa is a Public torrent site focused on adult Eastern Asian media including anime, manga, games and JAV",
				 language: "en-US",
				 module: %{search: &SukebeiNyaa.search/1,
					   torrent_info: &SukebeiNyaa.get_torrent_info/1}}
  }

  def get_trackers do
    @trackers
    |> Map.keys
    |> Enum.map(&to_string(&1))
  end

  def get_tracker(board_id) do
    @trackers[board_id]
  end

  def with_tracker(tracker, func, args) do
    apply(get_tracker(String.to_atom(tracker))[:module][func], args)
  end

  def parse_html(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
	case Floki.parse_document(body) do
	  {:ok, parsed} -> parsed
	  _ -> false
	  end

	{:ok, %HTTPoison.Response{status_code: 404, body: _body}} ->
	  false

      _ -> false
    end
  end

  def parse_json(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
	case Jason.decode(body) do
	  {:ok, parsed} -> parsed
	  _ -> false
	end

      {:ok, %HTTPoison.Response{status_code: 404, body: _body}} ->
	false

      _ -> false
    end
  end

  def search_tracker(tracker_id, query) do
    if get_tracker(String.to_atom(tracker_id)) do
      %{String.to_atom(tracker_id) => with_tracker(tracker_id, :search, [query])}
    end
  end

  def torrent_info(tracker_id, query) do
    if get_tracker(String.to_atom(tracker_id)) do
      %{String.to_atom(tracker_id) => with_tracker(tracker_id, :torrent_info, [query])}
    end
  end

  def search_tracker_list(trackers, query) do
    if is_list(trackers) do
      for tracker <- trackers do
	Task.async(fn -> search_tracker(tracker, query) end)
      end
      |> Enum.map(
      fn res ->
	try do
	  Task.await(res, :infinity)
	catch
          _value -> nil
	end
      end)
  end
  end
  
end
